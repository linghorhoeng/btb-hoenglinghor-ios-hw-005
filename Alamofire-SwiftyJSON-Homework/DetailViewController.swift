//
//  DetailViewController.swift
//  Alamofire-SwiftyJSON-Homework
//
//  Created by Hoeng Linghor on 12/8/20.
//

import UIKit
import Alamofire
import SwiftyJSON

class DetailViewController: UIViewController{
    
    @IBOutlet weak var detailDes: UILabel!
    @IBOutlet weak var detailTitle: UILabel!
    
    var article : Article?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if let article = article{
            detailTitle.text = article.title
            detailDes.text = article.description
            print(article.id)
            
        }
    }
    
    @IBAction func deleteBtn(_ sender: Any) {
        let alert = UIAlertController(title: "", message: "Are you sure to delete this article?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { _ in
            
            let id = self.article?.id
            print(id)
            AF.request("http://110.74.194.124:3000/api/articles/\(id!)", method: .delete).responseJSON(completionHandler:{ response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let article = Article(json: json["data"])
                case .failure(_):
                    print("Fetch Failed")
                }
                
            })
            let firstView = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController

            self.navigationController?.pushViewController(firstView, animated: true)
            print("=======delete successfully=======")
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

