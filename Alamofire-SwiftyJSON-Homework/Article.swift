//
//  Article.swift
//  Alamofire-SwiftyJSON-Homework
//
//  Created by Hoeng Linghor on 12/7/20.
//

import Foundation
import SwiftyJSON

class Article {
    var id:String?
    var title:String?
    var description:String?

    
    init() {}
    
    init(json:JSON) {
        self.id = json["_id"].stringValue
        self.title = json["title"].stringValue
        self.description = json["description"].stringValue
    }
}
