//
//  ArticleTableViewCell.swift
//  Alamofire-SwiftyJSON-Homework
//
//  Created by Hoeng Linghor on 12/8/20.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {

    @IBOutlet weak var textDes: UILabel!
    @IBOutlet weak var textTitle: UILabel!
    var article : Article?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        textTitle.text = article?.title
        textDes.text = article?.description
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
