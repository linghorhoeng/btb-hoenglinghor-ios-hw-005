//
//  ViewController.swift
//  Alamofire-SwiftyJSON-Homework
//
//  Created by Hoeng Linghor on 12/7/20.
//

import UIKit
import SwiftyJSON
import Alamofire

class ViewController: UIViewController {
    
    @IBOutlet weak var tbView: UITableView!
    
    var article = [Article]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
        // Do any additional setup after loading the view.
        tbView.register(UINib(nibName: "ArticleTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
    }
    func getData() {
        AF.request("http://110.74.194.124:3000/api/articles").responseJSON { reponse in
            switch reponse.result{
            case .success(let value):
                var articles: [Article] = []
                let json = JSON(value)
                for (key, subJson):(String, JSON) in json["data"] {
                    articles.append(Article(json: subJson))
                    print(subJson)
                }
                print(articles)
                self.article = articles
                self.tbView.reloadData()
                
            case .failure(_):
                print("Fetch Failed")
            }
        }
    }
    func deleteData(id: String) {
        AF.request("http://110.74.194.124:3000/api/articles/\(id)", method: .delete).responseJSON(completionHandler:{ response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let article = Article(json: json["data"])
                self.getData()
                self.tbView.reloadData()
            case .failure(_):
                print("Fetch Failed")
            }
            
        })
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            let dest = segue.destination as! DetailViewController
            dest.article = sender as? Article
        }
    }
}
extension ViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return article.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ArticleTableViewCell
        cell.textTitle.text = self.article[indexPath.row].title
        cell.textDes.text = self.article[indexPath.row].description
        //        cell.textLabel?.text = self.article[indexPath.row].title
        //        cell.textLabel?.text = self.article[indexPath.row].description
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        100
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let article = self.article[indexPath.row]
        self.performSegue(withIdentifier: "showDetail", sender: article)
    }
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: nil) {(action,view,Bool) in
            
            let article = self.article[indexPath.row]
            self.deleteData(id: self.article[indexPath.row].id!)
            print("=======delete successfully=======")
            self.tbView.reloadData()
        }
        delete.image = UIImage(systemName: "trash")
        return UISwipeActionsConfiguration(actions: [delete])
        
    }
    func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        let actionProvider: UIContextMenuActionProvider = { _ in
            let editMenu = UIMenu(title: "Edit...",image: UIImage(systemName: "pencil"), children: [
                UIAction(title: "Copy") { _ in },
                UIAction(title: "Duplicate") { _ in }
            ])
            return UIMenu(title: "What do you want to do?", children: [
                UIAction(title: "Delete",image: UIImage(systemName: "trash"),attributes: UIMenuElement.Attributes.destructive) { _ in
                    let alert = UIAlertController(title: "", message: "Are you sure?", preferredStyle: .actionSheet)
                    alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { _ in
                        self.deleteData(id: self.article[indexPath.row].id!)
                        print("=======delete successfully=======")
                        self.tbView.reloadData()
                        
                    }))
                    alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                },
                UIAction(title: "View Detail",image: UIImage(systemName: "eye")){
                    _ in
                    let article = self.article[indexPath.row]
                    self.performSegue(withIdentifier: "showDetail", sender: article)
                    //            let detailViewController = self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
                    //            self.navigationController?.pushViewController(detailViewController, animated: true)
                },
                editMenu
            ])
        }
        
        return UIContextMenuConfiguration(identifier: "unique-ID" as NSCopying, previewProvider: nil, actionProvider: actionProvider)
    }
}
